// SFML_CHESS.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <Windows.h>
#include <string>
#include "Connector.hpp"


#define DEBUG 0

//some globals

int figure_size = 56;


sf::Sprite figures[32];


std::string to_chess_note(sf::Vector2f pos) {

#if DEBUG==1
	std::cout << "to_chess_note()";
#endif
	std::string s("");
	s += char(pos.x / figure_size + 97);
	s += char(7 - pos.y / figure_size + 49);
	return s;

}


sf::Vector2f to_coord(char a, char b) {

#if DEBUG==1
	std::cout << "to_coord()";
#endif
	int x(int(a) - 97), y(7 - int(b) + 49);
	return sf::Vector2f(x*figure_size, y*figure_size);
}



void move(std::string str) {

#if DEBUG==1
#endif
	std::cout << "move()" << "str=" << str;

	sf::Vector2f old_pos(to_coord(str[0], str[1]));
	sf::Vector2f new_pos(to_coord(str[2], str[3]));
	for (int i = 0; i < 32; i++) {
		if (figures[i].getPosition() == new_pos) {
			figures[i].setPosition(-100, -100);
		}
	}

	for (int i = 0; i < 32; i++)
		if (figures[i].getPosition() == old_pos)
			figures[i].setPosition(new_pos);
}



int board[8][8] = {
	-1,-2,-3,-5,-4,-3,-2,-1,
	-6,-6,-6,-6,-6,-6,-6,-6,
	 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0,
	 6, 6, 6, 6, 6, 6, 6, 6,
	 1, 2, 3, 5, 4, 3, 2, 1,
};

std::string move_history = "";

void loadmove_history() {
#if DEBUG==1
	std::cout << "loadmove_history()";
#endif
	int k = 0;
	for(int i=0;i<8;i++)
		for (int j = 0; j < 8; j++) {
			int n = board[i][j];
			if (!n)
				continue;
			int x = abs(n) - 1;
			int y = n > 0 ? 1 : 0;
			figures[k].setTextureRect(sf::IntRect(figure_size*x, figure_size*y, figure_size, figure_size));
			figures[k++].setPosition(figure_size*j, figure_size*i);
		}

	//std::cout << "\nmove_history.lenght()=" << move_history.length();
	for (int i = 0; i < move_history.length(); i += 5) {
		move(move_history.substr(i, 4));
		//std::cout << "it_happens="<<move_history.substr(i,4)<<'\n';
	}
}





int main(){
	//let's make a chess game
	//the AI will take over
	char c[] = "stockfish.exe";

	//the main window
	sf::RenderWindow main_window(sf::VideoMode(453, 453), "You will lose against the AI");

	//the texture for figures
	sf::Texture figure_texture,board_texture;
	figure_texture.loadFromFile("images/figures.png");
	board_texture.loadFromFile("images/board0.png");


	//the sprite for figures
	sf::Sprite sprite_figure(figure_texture),sprite_board(board_texture);


	for (int i = 0; i < 32; i++)
		figures[i].setTexture(figure_texture);

	loadmove_history();

	bool is_moving = false;

	float dx = 0.f, dy = 0.f;
	sf::Vector2f fig_old_pos, fig_new_pos;
	std::string str_note("");
	int last_moved_figure;

	ConnectToEngine(c);
	while (main_window.isOpen()) {
		sf::Vector2i mouse_pos = sf::Mouse::getPosition(main_window);

		sf::Event event_listener;
		while (main_window.pollEvent(event_listener)) {

			if (sf::Event::KeyPressed == event_listener.type) {
				if (sf::Keyboard::BackSpace == event_listener.key.code) {
						move_history.erase(move_history.length() - 6, 5);
						loadmove_history();
				}
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
				str_note = get_next_move(move_history);
				if (str_note == "error") {
					std::cout << "You have beaten the AI!\nCongrats!!";
					exit(-2);

				}
				fig_old_pos = to_coord(str_note[0], str_note[1]);
				fig_new_pos = to_coord(str_note[2], str_note[3]);

				for (int i = 0; i < 32; i++)
					if (figures[i].getPosition() == fig_old_pos)
						last_moved_figure=i;
				for (int k = 0; k < 100; k++) {
					sf::Vector2f p = fig_new_pos - fig_old_pos;
					figures[last_moved_figure].move(p.x / 100, p.y / 100);
					main_window.draw(sprite_board);
					for (int i = 0; i < 32; i++)
						main_window.draw(figures[i]);
					main_window.draw(figures[last_moved_figure]);
					main_window.display();
				}
				move(str_note);
				move_history += str_note + "";
				figures[last_moved_figure].setPosition(fig_new_pos);
			}

			if (sf::Event::Closed == event_listener.type) {
				main_window.close();
				CloseConnection();
				exit(-1);
			}
			if (sf::Event::MouseButtonPressed == event_listener.type) {
				if (event_listener.key.code == sf::Mouse::Left) {
					for (int i = 0; i < 32; i++) {
						if (figures[i].getGlobalBounds().contains(mouse_pos.x, mouse_pos.y)) {
							is_moving = true; last_moved_figure = i;
							dx = mouse_pos.x - figures[i].getPosition().x;
							dy = mouse_pos.y - figures[i].getPosition().y;
							fig_old_pos = figures[i].getPosition();
						}
					}
				}
			}

			if (sf::Event::MouseButtonReleased == event_listener.type) {
				//align to the chessboard grid
				is_moving = false;
				sf::Vector2f raw_pos = figures[last_moved_figure].getPosition() + sf::Vector2f(figure_size / 2, figure_size / 2);
				sf::Vector2f aligned_pos(figure_size*int(raw_pos.x / figure_size), figure_size*int(raw_pos.y / figure_size));
				fig_new_pos = aligned_pos;
				str_note = to_chess_note(fig_old_pos) + to_chess_note(fig_new_pos);
				std::cout << '\n'<<str_note << '\n';
				move(str_note);//destroy figures that get caught in moves
				move_history += str_note + " ";
				std::cout << "\nmove_history=" << move_history;
				figures[last_moved_figure].setPosition(fig_new_pos);//align the new figure
				//to the grid
			}
		}
		if (is_moving)
			figures[last_moved_figure].setPosition(mouse_pos.x - dx, mouse_pos.y - dy);

		main_window.clear();
		main_window.draw(sprite_board);
		for (int i = 0; i < 32; i++)
			main_window.draw(figures[i]);

		main_window.display();
	}
	return 0x0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
